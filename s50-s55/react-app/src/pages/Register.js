import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import { useNavigate, Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function Register() {


	const navigate = useNavigate();

	// state hooks to store the values of the input fields
	const [ email, setEmail ] = useState('');
	const [ password1, setPassword1 ] = useState('');
	const [ password2, setPassword2 ] = useState('');
	// State to determine whether the submit button is enabled or not
	const [ isActive, setIsActive ] = useState(false);
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [mobileNo, setMobileNo] = useState('');

	async function registerUser(e) {
		e.preventDefault();

		try {
		    // Make an API call to check if the email already exists
		    const emailCheckResponse = await fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
	            method: 'POST',
	            headers: {
	                'Content-Type': 'application/json',
	            },
	            body: JSON.stringify({
	                email: email,
	            }),
	        });

	        const emailCheckData = await emailCheckResponse.json();

	        if (emailCheckResponse.status === 200) {
			    if (emailCheckData.emailExists) {
			        Swal.fire({
			            title: "Registration Failed",
			            icon: "error",
			            text: "Email already exists. Please use a different email.",
			        });
			        return;
			    }
			}

		    const response = await fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
		        method: 'POST',
		        headers: {
		            'Content-Type': 'application/json',
		        },
		        body: JSON.stringify({
		        	firstName: firstName,
				    lastName: lastName,
				    email: email,
				    mobileNo: mobileNo,
				    password: password1,
				    address: "",
				    age: ""
		        }),
		    });

		    if (response.status === 200) {			    
			    setEmail('');
		        setPassword1('');
		        setPassword2('');
		        setFirstName('');
		        setLastName('');
		        setMobileNo('');
		        Swal.fire({
		            title: "Registered Successfully",
		            icon: "success",
		            text: "Welcome to Zuitt!"
		        })
		        navigate('/login');
		    } else if (response.status === 404) {
		        Swal.fire({
		            title: "Registration Failed",
		            icon: "error",
		            text: "Please check your registration details and try again!"
		        })
		    }
		} catch (error) {
		    console.error('Error registering user:', error);
		    Swal.fire({
	            title: "Registration Failed",
	            icon: "error",
	            text: "Please try again!"
	        })
		}
	}


	useEffect(() => {
    // Validation to enable the submit button when all fields are populated and both passwords match
    if (
        email !== "" &&
        password1 !== "" &&
        password2 !== "" &&
        firstName !== "" &&
        lastName !== "" &&
        mobileNo.length >= 10 &&
        password2 === password1
    ) {
        setIsActive(true);
    } else {
        setIsActive(false);
     }
    }, [email, password1, password2, firstName, lastName, mobileNo]);


	// function registerUser(e) {
	// 	e.preventDefault();

	// 	// Write a code that empty the input field for email, password1, password2

	// 	setEmail('');
	// 	setPassword1('');
	// 	setPassword2('');
	// 	alert("Thank You for registering!");

	// };

	return (
		<Form onSubmit={(e) => registerUser(e)}>
			<Form.Group controlId="firstName">
			  <Form.Label>First Name</Form.Label>
			  <Form.Control
			    type="text"
			    placeholder="Enter first name here"
			    value={firstName}
			    onChange={e => setFirstName(e.target.value)}
			    required
			  />
			</Form.Group>

			<Form.Group controlId="lastName">
			  <Form.Label>Last Name</Form.Label>
			  <Form.Control
			    type="text"
			    placeholder="Enter last name here"
			    value={lastName}
			    onChange={e => setLastName(e.target.value)}
			    required
			  />
			</Form.Group>

			<Form.Group controlId="email">
				<Form.Label>Email Address</Form.Label>
				<Form.Control
					type="email"
					placeholder="Enter email here"
					value={email}
					onChange={e => setEmail(e.target.value)}
					required
				/>
				<Form.Text className="text-muted">
					We'll never share your email with anyone else.
				</Form.Text>
			</Form.Group>

			<Form.Group controlId="mobileNo">
			    <Form.Label>Mobile Number</Form.Label>
			    <Form.Control
			        type="tel"
			        placeholder="Enter mobile number here"
			        value={mobileNo}
			        onChange={e => setMobileNo(e.target.value)}
			        required
			    />
			  	<Form.Text className="text-muted">
					We'll never share your mobile number with anyone else.
				</Form.Text>
			</Form.Group>

			<Form.Group controlId="password1">
				<Form.Label>Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Password"
					value={password1}
					onChange={e => setPassword1(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group controlId="password2">
				<Form.Label>Verify Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Verify Password"
					value={password2}
					onChange={e => setPassword2(e.target.value)}
					required
				/>
			</Form.Group>
			{ isActive ? (
				<Button variant = "primary" type="submit" id="submitBtn">
					Submit
				</Button>
				) : (
				<Button variant = "danger" type="submit" id="submitBtn" disabled>
					Submit
				</Button>
				)
			}

		</Form>
	)
}
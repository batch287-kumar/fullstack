import Banner from '../components/Banner';
export default function Error(){

	const data = {
		title: "Error 404 - Page Not Found",
		content: "The page you are looking cannot be found.",
		destination: "/",
		Label: "Back to Home"
	}

	return (
		// <>
		// <h2>Error 404 - Page Not Found!</h2>
		// <p>The page you are looking cannot be found.</p>
		// <p>Go back to <a href="/">homepage</a></p>
		// </>

		<Banner data = {data} />
	)
}
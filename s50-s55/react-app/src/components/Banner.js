import { Button, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';

// export default function Banner() {
// 	return(
// 			<Row>
// 				<Col>
// 					<h1>Zuitt Coding Bootcamp</h1>
// 					<p>Opportunities for everyone, everywhere!</p>
// 					<Button variant="primary">Enroll Now!</Button>
// 				</Col>
// 			</Row>
// 		)
// }

export default function Banner({data}) {

	const { title, content, destination, label } = data;
	return(
			<Row>
				<Col>
					<h1>{title}</h1>
					<p>{content}</p>
					<Button variant="primary" as={Link} to={destination}>{label}</Button>
				</Col>
			</Row>
		)
}